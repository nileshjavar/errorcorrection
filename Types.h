#ifndef CUSTOM_TYPES_H
#define CUSTOM_TYPES_H

#include<vector>
#include<string>

using namespace std;
class wordTagListBitSequence{
	public :
		wordTagListBitSequence(){};
		~wordTagListBitSequence(){};
		vector<string> words;
		vector< vector<string> > tagList;
		string bitSequence;
};

class maxValue{
	public :
		maxValue(){
			row = -1;
			column = -1;
			bitSequence = "";
		};
		~maxValue(){};
		int row;
		int column;
		string bitSequence;
		void clear(){
			row = -1;
			column = -1;
			bitSequence = "";
		};
};

#endif
