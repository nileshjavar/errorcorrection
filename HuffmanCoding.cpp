#include "globals.h"
#include "HuffmanCoding.h"
#include<iostream>
#include<stdlib.h>
#include<fstream>

HuffmanCoding * HuffmanCoding::mInstance = NULL;
HuffmanCoding::hNode::hNode(string inS, int inFrequency, hNode * inLeft, hNode * inRight) {
	s = inS;
	frequency = inFrequency;
	left = inLeft;
	right = inRight;
}

HuffmanCoding::llNode::llNode(hNode * inNode, llNode * inNext){
	node = inNode;
	next = inNext;
}

HuffmanCoding::HuffmanCoding(){
	LOG("Hello World!!"<<endl);
	root = NULL;
	hNodeArray = NULL;
	sSize = 0;
	PEB = 3; //initializing it to 5%
	initialize();	
}

HuffmanCoding::~HuffmanCoding(){
	clearHNodeArray();
}

void HuffmanCoding::initialize(){
		//TODO : change the data structure of code from map<string,string> to map<char, string>

	retrieveSymbols();
	LOG("Symbol Frequency size "<<symbolFrequency.size()<<endl);
	sSize = symbolCount;
	populateHNode();

	//sorting to create the initial list of sorted elements
	quickSort(hNodeArray, 0, symbolCount);
	createLinkedList();
	createTree();
	LOG("Generating Codes"<<endl);
	generateCodes(root->node, "" );
	LOG("Codes generated"<<endl);
	printCodes(&symbolCodes);

       	/*string largestWord("");
	LOG(getLengthOfLargestToken(largestWord));*/
	string str("nilesh kumar javar");
	string encodedSequence;
	encode(str, encodedSequence);
	string decodedString;
	decode(encodedSequence, decodedString);
	dumpWordList();
}

void HuffmanCoding::dumpWordList(){
	Json::Value rootJson;
	Json::Reader reader;
	std::ifstream test("/home/javar/Desktop/Dropbox/Thesis/Programming/NLP2/fullIndex.txt", std::ifstream::binary);
	bool parsingSuccess = reader.parse(test,rootJson,false);		
	if(!parsingSuccess){
		LOG("Fail"<<endl);
		return;
	}
	Json::Value wordFrequencies = rootJson["wordFrequencies"];

	vector<string> keys = wordFrequencies.getMemberNames();
	std::fstream words("/home/javar/Desktop/Dropbox/Thesis/Programming/NLP2/wordList.txt", std::fstream::out);
	for( int i = 0;i< keys.size();i++)
		words<<keys[i]<<endl;
	words.close();

}
void HuffmanCoding::printCodes( symbolCodesMap * symbolCodes){
	 for (symbolCodesMap::iterator it=(*symbolCodes).begin(); it!=(*symbolCodes).end(); ++it)
    		logFile << it->first << " => " << it->second << '\n';
}

void HuffmanCoding::printLL(llNode * r){
	LOG("printing LinkedList"<<endl);
	while( r!=NULL ){
		LOG(r->node->s<<" "<<r->node->frequency<<endl);					
		r = r->next;
	}
	LOG("End: Printing Linkedlist"<<endl);
}

void HuffmanCoding::print(hNode **a, int size){
	LOG("Size = "<<size<<endl);
	for(int i = 0; i< size;i++){
		LOG("Print : 1"<<endl);
		LOG(a[i]->s<<" "<<a[i]->frequency<<endl);
	}
	LOG("Print : 2"<<endl);
	LOG(endl);
}

void HuffmanCoding::quickSort( hNode ** a, int start, int end){
	//LOG("From QuickSort  : ");
	//print(a, sSize);
	if(start < end ){
		int p = partition(a,start,end);
		quickSort(a,start,p);
		quickSort(a,p+1, end);
	}
}

int HuffmanCoding::partition(hNode ** a, int start, int end){
	int k = start-1;
	if(start < end){
		//print(a,sSize);
		int pivot = a[end-1]->frequency;
		hNode * temp;
		for(int i = start; i<end-1;i++){
			if(a[i]->frequency <= pivot){
				k++;
				temp = a[k];
				a[k] = a[i];
				a[i] = temp;
			}
		}
		temp = a[end-1];
		a[end-1]= a[k+1];
		a[k+1] = temp;
		//print(a,sSize);
	}
	return k+1;	
}

void HuffmanCoding::populateHNode()
{
	symbolFrequencyMap * symbolFrequencyPtr = &symbolFrequency; 
	symbolCount = symbolFrequencyPtr->size();
		
	hNodeArray = new hNode*[symbolCount];
	int i = 0;
	for(map<string,int>::iterator it = (symbolFrequencyPtr)->begin(); it!=(symbolFrequencyPtr)->end();++it){
		hNodeArray[i] = new hNode(it->first, it->second, NULL, NULL);
		i++;
	}
	if(i!=symbolCount){
		LOG("PopulateHNode::Fatal error occured. Exiting"<<endl);
	}
}

void HuffmanCoding::clearHNodeArray()
{
	for(int i=0;i<symbolCount;i++){
		if(hNodeArray[i]){
			delete hNodeArray[i];
			hNodeArray[i] = NULL; 
		}
	}
	if(hNodeArray){
		delete [] hNodeArray;	
		hNodeArray = NULL;
	}
}

void HuffmanCoding::createLinkedList(){
	LOG("Creating Linkedlist"<<endl);
	llNode * prevNode = NULL;					
	for(int i = 0;i<symbolCount;i++){
		if(root == NULL ){
			LOG("Root node created"<<endl);
			root = new llNode(hNodeArray[i], NULL);
			prevNode = root;
			continue;
		}
		prevNode->next = new llNode(hNodeArray[i], NULL);
		prevNode = prevNode->next;
	}
	LOG("linkedlist created"<<endl);		
}


void HuffmanCoding::createTree(){
	LOG("Creating the Tree"<<endl);
	llNode * firstSmallest = NULL;
	llNode * secondSmallest = NULL;		
	//temp is used to store the newly created tree node
	llNode * temp = NULL;
	llNode * temp1 = NULL;
	//temp2 is to store the root so that insertion of the new node can be done
	llNode * temp2 = NULL;
	string symbols("");
	int frequency = 0;
	//LOG("***Size1 of queue = "<<sizeOfQueue()<<endl);
	while( sizeOfQueue() > 1){
		//LOG("Hello WOrld"<<endl);
		//LOG("***Size of Queue = "<<sizeOfQueue()<<"  "<<endl);
		//printLL(root);
		firstSmallest = (root);
		secondSmallest = (root)->next;
		symbols = firstSmallest->node->s + secondSmallest->node->s;
		frequency = firstSmallest->node->frequency + secondSmallest->node->frequency;
		temp = new llNode(new hNode(symbols, frequency, firstSmallest->node, secondSmallest->node), NULL);		
		temp2 = root;
		//Inseritng the new node
		while( temp2->next && temp2->next->node->frequency < frequency ){
			//LOG("Checking for next node"<<endl);
			temp2 = temp2 -> next;
		}
		llNode * temp3 = root;
		if(temp2 == NULL){
			while(temp3->next!=NULL)
				temp3 = temp3->next;	
			temp3->next = temp;
		}else{
			temp3 = temp2->next;
			temp2->next = temp;
			temp->next = temp3;
		}
		/*if(temp)
			LOG("Inserted new node : "<<temp->node->s<<" freq = "<<temp->node->frequency<<" left = "<< temp->node->left->s<<" right = "<<temp->node->right->s<<endl);*/
		//deleting the first two nodes
		int countOfNodes = 2;
		while( countOfNodes>0){
			temp = root;	
			root = (root)->next;
			if(temp){
				delete temp;
				temp = NULL;
			}
			countOfNodes--;
		}
	}
	LOG("Tree Created"<<endl);	
}

int HuffmanCoding::sizeOfQueue(){
	int count = 0;
	llNode * r = root;
	while(r!=NULL){
		count = count + 1;	
		r = r->next;
	}
	return count;
}

void HuffmanCoding::generateCodes(hNode * root, string s){
	if(root->left == NULL && root->right == NULL){
		symbolCodes[root->s] = s;
		return;
	}	
	generateCodes(root->left, s+"0");
	generateCodes(root->right, s+"1");
}

void HuffmanCoding::retrieveSymbols(){
	Json::Value root;
	Json::Reader reader;
	std::ifstream test("/home/javar/Desktop/Dropbox/Thesis/Programming/NLP2/fullIndex.txt", std::ifstream::binary);
	bool parsingSuccess = reader.parse(test,root,false);		
	if(!parsingSuccess){
		LOG("Fail"<<endl);
		return;
	}
	const Json::Value symbolFrequencyJson = root["symbolFrequency"];
	vector<string> keys = symbolFrequencyJson.getMemberNames();
	for(int i = 0;i<keys.size();i++){
		(symbolFrequency)[keys[i]] = (symbolFrequencyJson[keys[i]]).asInt();
	}
	LOG("count of symbols = "<<keys.size()<<endl);
}

int HuffmanCoding::getLengthOfLargestToken(string &word){
	Json::Value rootJson;
	Json::Reader reader;
	std::ifstream test("/home/javar/Desktop/Dropbox/Thesis/Programming/NLP2/fullIndex.txt", std::ifstream::binary);
	bool parsingSuccess = reader.parse(test,rootJson,false);		
	if(!parsingSuccess){
		LOG("Fail"<<endl);
		return 0;
	}
	Json::Value wordFrequencies = rootJson["wordFrequencies"];

	vector<string> keys = wordFrequencies.getMemberNames();
	LOG("Number of words  = "<<keys.size()<<" ");
	vector<string> wordList;
	int maxLength = 0;
	int length = 0;
	string symbol("");
	int count = 0;
	for(int i = 0;i <keys.size() ; i++){
		count++;
		wordList.clear();
		split(keys[i],"-", wordList);
		
		for(int k = 0;k<wordList.size(); k++){
			length = 0;
			for(int j = 0; j < wordList[k].size(); j++){
				symbol = wordList[k][j];
				//LOG(keys[i][j]<<" "<<symbolCodes[symbol]<<endl);
				length = length + (symbolCodes[symbol]).size();		
			}
			if(length > maxLength){
				maxLength = length;
				word = keys[i];
			}
		}
	}
	LOG("Word = "<<word<<" maxLength = "<<maxLength<<endl);
	LOG("Count = "<<count<<endl);
	return maxLength;
}

void HuffmanCoding::split( const string & word, const string & delimiter, vector<string> & wordList){
	string s("");
	char k = '\0';
	for(int i = 0;i<word.length();i++){
		k = (char)word[i];
		if(!((k >= 48 && k <=57) || (k>=64 && k<=90) || (k>=97 && k<=122))){
			wordList.push_back(s);
			wordList.push_back(string(1,k));
			s = "";
		}else{
			s=  s + word[i];
		}
	}
	if(s.length() > 0 ){
		wordList.push_back(s);
		s="";
	}
}

void HuffmanCoding::encode(const string & inWord, string & outBitSequence){
	int i = 0;
	string temp;
	string bitSequence;
	while( i < inWord.size()){
		temp = inWord[i];
		outBitSequence = outBitSequence + symbolCodes[temp];
		i++;
	}
	LOG(outBitSequence<<endl);
	return;
}

void HuffmanCoding::decode(const string & inBitSequence, string & outWord){
	int i = 0;
	bool isSymbolFound = false;
	hNode * treeNode = NULL;
	while(i < inBitSequence.size()){
		if( (i == 0) || isSymbolFound )	{
			treeNode = root->node;
			isSymbolFound = false;
		}
		if(treeNode == NULL){
			LOG("NULL root seen in the middle of parsing : Invalid Sequence"<<endl);
			outWord = "";
			return;
		} else if(treeNode->left == NULL && treeNode->right == NULL){
			outWord = outWord + treeNode->s;
			isSymbolFound = true;
			continue;	
		}
		if( inBitSequence[i] == '1')
			treeNode = treeNode->right;
		else
			treeNode = treeNode->left;
		i++;
	}
	if( treeNode->left == NULL && treeNode->right == NULL)
		outWord = outWord + treeNode->s;
	else {
		outWord = "";
		//LOG("incomplete outWord = "<<outWord<<endl);
		//LOG(treeNode->s<<endl);	
		//LOG("NULL root not present at the end of parsing : Invalid Sequence"<<endl);
		//outWord ="";
	}
	//LOG("Decoded Word = "<<"$$"<<outWord<<"$$"<<endl);
	return;		
}

void HuffmanCoding::encodeAndInduceErrors(fstream & inFile, fstream & outFile){
	string line;
	string temp;
	string code;
	while( getline(inFile, line) ){
		for(int i = 0;i<line.size();i++){
			temp = line[i];
			code = symbolCodes[temp];
			for(int j = 0; j<code.size();j++){
				if( ( ( (rand() % 100) + 1 )% ( 100/PEB ) ) == 0  )
					outFile<<'$';
				else	
					outFile<<code[j];
			}
		}
	}
}

void HuffmanCoding::encodeAndInduceErrors(const string & line, string & outSentence){
	string temp;
	string code;
	outSentence = "";
	for(int i = 0;i<line.size();i++){
		temp = line[i];
		code = symbolCodes[temp];
		for(int j = 0; j<code.size();j++){
			if( ( ( (rand() % 100) + 1 )% ( 100/PEB ) ) == 0  )
				outSentence +='$';
			else	
				outSentence +=code[j];
		}
	}
}










