#ifndef TOKENPROCESSOR_H
#define TOKENPROCESSOR_H

#include<iostream>
#include<string>
#include<vector>
#include<fstream>
#include "json/json.h"
#include "Types.h"
#include<map>

using namespace std;
typedef vector<string> wordVec;
typedef vector< vector<string> > vecTagVec;
typedef vector<string> tagVec;
typedef map<string, int> matrixMap;
class TokenProcessor{
	public :
		struct TrieNode{
			vector<string> * tags;
			TrieNode * next;
			TrieNode * children;
			char s;
		};
		static TrieNode * root;
		static Json::Value jsonRoot;
		static matrixMap tmMap;
		static matrixMap emMap;
		static matrixMap smMap;
		static tagVec masterTagsList;
		static bool isInitialized;
		TokenProcessor();
		~TokenProcessor();
		void tokenize( const string & inSentence, vector<string> & outTokens);
		bool isNumber(const string & inWord);
		void createAllBitSequences(const string & inBitSequence, char ** outBitSequences, size_t count );
		void createTokens(const string & inBitSequences, vector<string> & outTokenVector, vector < wordTagListBitSequence * >  &);
		const vector<string> * isValidWord(const string & inWord);
		
		//given the list of token tag sequences finds the sequence
		//with Maximum probability.
		void findWTSequence( wordVec & inWordVec, 
					vecTagVec & inVecTagVec,
					const string & inStartTag,
					const string & inEndTag,
					tagVec & inTagVec,
					int inOutLevel,
					double & inOutMaxProb );
		
		double findProbabilityWTSequence( wordVec & inWordVec, 
						  const string & inStartTag,
						  const string & inEndTag,
						  tagVec & inTagVec);

		/*static void createTransitionMatrixMap();
		static void createEmissionMatrixMap(); 
		static void createStartingMatrixMap();*/
		static void initialize(); 

	private :
		/*static TrieNode * root;
		static Json::Value jsonRoot;
		static matrixMap tmMap;
		static matrixMap emMap;
		static matrixMap smMap;
		static tagVec masterTagsList;*/

		static void createWordTagsTrie();
		static void createTransitionMatrixMap();
		static void createEmissionMatrixMap(); 
		static void createStartingMatrixMap(); 
		void clearTrie(TrieNode * root);
};
#endif
