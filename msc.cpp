/**
  * File : Corrects the senetnces by reading one after the other from the file.
  * Compiling : g++ globals.h Types.h HuffmanCoding.cpp TokenProcessor.cpp Viterbi.cpp msc.cpp -I./json/include -ljson
  * msc stands for manySentencesCorrection.. The original name of the file.			
  */

#include<iostream>
#include<fstream>
#include<string>
#include<sys/time.h>
#include "Viterbi.h"
using namespace std;

int main()
{
	string path("/media/javar/4AE5-AF1B/results/");
	string sentence("");
	string errorBitSequence("");
	string errorSentence("");
	string errorInducedBitSequence("");
	Viterbi * pViterbi = new Viterbi();
	
	fstream inputFile;
	fstream correctFile;
	fstream errorFile;
	fstream countSoFar;	
	int isCorrected;
	int count= 0;	
	int countOfCorrectedFiles = 0;
	int countOfErrorFiles = 0;
	int countOfTimeOutFiles = 0;
	inputFile.open((path+"validSent.txt").c_str());
	//inputFile.open("/home/javar/Desktop/Dropbox/Thesis/Programming/NLP2/hufmanCoding/FilesUtility/TimeOutSentences.txt");
	correctFile.open((path+"corrected.txt").c_str());
	errorFile.open((path+"error.txt").c_str());
	countSoFar.open((path+"count.txt").c_str());
	struct timeval tim;
	double startTime = 0;
	double endTime = 0;
	while( getline(inputFile, sentence) ){
		gettimeofday(&tim,NULL);
		startTime = tim.tv_sec;
		//cout<<"Count = "<<count<<endl;
		/*if( !(count > 4 && count <9) ){
			count++;
			continue;	
		}*/
		//cout<<"Count = "<<count<<endl;
		//cout<<"sentence = "<<sentence<<endl;
		errorBitSequence = "";
		errorSentence = "";
		errorInducedBitSequence = "";
		isCorrected = 0;
		isCorrected = pViterbi->correctTheSentence(sentence, errorInducedBitSequence, errorBitSequence, errorSentence, count);
		if(isCorrected == 2){
			correctFile<<count<<" : "<<sentence<<endl;
			countOfCorrectedFiles++;
		}else if(isCorrected == 0) {
			countOfErrorFiles++;
			errorFile<<count<<" : "<<"Actual Sentence = "<<sentence<<endl;
			errorFile<<count<<" : "<<"error induced bit Sequence"<<errorInducedBitSequence<<endl;
			errorFile<<count<<" : "<<"error Bit Sequence = "<<errorBitSequence<<endl;
			errorFile<<count<<" : "<<"error Sentence = "<<errorSentence<<endl;
			errorFile<<"----------------------------------------------"<<endl;
		} else {
			countOfTimeOutFiles++;
		  	errorFile<<count<<" : "<<"Actual Sentence = "<<sentence<<endl;
			errorFile<<count<<" : "<<"error induced bit Sequence"<<errorInducedBitSequence<<endl;
			errorFile<<count<<" : "<<"error Bit Sequence = "<<errorBitSequence<<endl;
			errorFile<<count<<" : "<<"error Sentence = "<<errorSentence<<endl;
			errorFile<<"----------------------------------------------"<<endl;
		}
		gettimeofday(&tim,NULL);
		endTime = tim.tv_sec;
		
		count++;
		countSoFar<<" Count = "<<count<< " Time Taken  = "<<endTime-startTime<<endl<<" Count of Corrected Files = " <<countOfCorrectedFiles<<endl<<" Count of Error Files = "<<countOfErrorFiles<<" Time out files count = "<<countOfTimeOutFiles<<endl;
		 	
	}
	//cout<<" Count = "<<count<<endl<<" Count of Corrected Files = " <<countOfCorrectedFiles<<endl<<" Count of Error Files = "<<countOfErrorFiles<<" Timeout count= "<<countOfTimeOutFiles<<endl; 	
	inputFile.close();
	correctFile.close();
	errorFile.close();
	countSoFar.close();
	delete pViterbi;
	return 0;
}
