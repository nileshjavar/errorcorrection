import nltk
import re
from nltk.corpus import brown
import cPickle as pickle
import json
from collections import OrderedDict as dict
sents =  brown.tagged_sents(simplify_tags=True)

#sents = brown.tagged_sents()
print "number of sentences = ",len(sents)
transitionMatrix = dict()
wordToTags = dict()
emissionMatrix = dict()
sentenceStartWordsToTags = dict()
startingMatrix = dict()
wordFrequency = dict()
fullIndex = dict()
symbolFrequency = dict()
sentenceCount = 0
total = "total"
completeIndexFile = open("fullIndex_simplifiedTagset.txt", "wb+")
sentenceEnd = dict();
lSentenceEnd = [];
wList = [];
tList = [];
wCount = 0;
mCount = 1000000;
alphabets = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
sentenceDelimiters = ['!',':',';','(',')','.','?','\'',',']
#sentenceDelimiters = ['.',';','?','*']
cSentenceCount = 0
startingMatrix["Total"] = dict();
startingMatrix["Total"] = 0;
while sentenceCount < len(sents) :
	print "sentence Count = ", sentenceCount 
	sentence = sents[sentenceCount]
	k = sentence[len(sentence)-1][0]
	k1 = k[len(k)-1]
	if not k1 in lSentenceEnd :
		lSentenceEnd.append(k1)
	#print sentence
	count = 0
	#if( sentence[len(sentence)-1][0] in sentenceDelimiters ) :
	if( True ) :	
		if( sentence[len(sentence)-1][0] in sentenceEnd ) :
			sentenceEnd[sentence[len(sentence)-1][0]] = sentenceEnd[sentence[len(sentence)-1][0]] + 1
		else :
			sentenceEnd[sentence[len(sentence)-1][0]] = 1
		cSentenceCount = cSentenceCount+1;
		while(count < len(sentence )) :
			word = sentence[count][0]
			tag = sentence[count][1]
			wCount = wCount + 1;
			mCount = mCount - 1;
			#stList = re.split('[;:.,*+-]+',tag)
					
			#print "tag = ",tag, "stLIst = ",stList
			if tag not in tList :
				tList.append(tag)
			if not word in sentenceDelimiters :
				if not word in wList :
					wList.append(word)
			i = 0
			while(i < len(word)) :
				if word == '!' or word == ':' :
					print "Found !,:, adding to frequencies"
				if word[i] in symbolFrequency :
					symbolFrequency[word[i]] = symbolFrequency[word[i]] + 1
				else :
					symbolFrequency[word[i]] = 1
				i = i + 1
			if word in wordFrequency :
					wordFrequency[word] = wordFrequency[word] + 1
			else :
					wordFrequency[word] = 1
			fL = word[0]
			if( len(word) == 1 and (fL in alphabets) ) :
				if( (fL!='I' and fL!='i' and fL!='o' and fL!='O' and fL!='a' and fL!='A' ) ) :
					count = count + 1;
					continue
			if(count == 0) :
				#word = sentence[0][0]
				#tag  = sentence[0][1]
				#Storing the starting frequencies
				if tag in startingMatrix :
					startingMatrix[tag] = startingMatrix[tag] + 1
				else :
					startingMatrix[tag] = 1

				startingMatrix["Total"] = startingMatrix["Total"] + 1

				#Storing the Frequency of the words
				
				if word in sentenceStartWordsToTags :
					if tag in sentenceStartWordsToTags[word] :
						sentenceStartWordsToTags[word][tag] = sentenceStartWordsToTags[word][tag] + 1
					else :
						sentenceStartWordsToTags[word][tag] = dict()
						sentenceStartWordsToTags[word][tag] = 1
				else :
					sentenceStartWordsToTags[word] = dict()
					sentenceStartWordsToTags[word][tag] = dict()
					sentenceStartWordsToTags[word][tag] = 1
			else :
				prev_word = sentence[count-1][0]
				prev_tag  = sentence[count-1][1]
				#word = sentence[count][0]
				#tag  = sentence[count][1]
				#adding to transition matrix
				if prev_tag in transitionMatrix :
					if tag in transitionMatrix[prev_tag] :
						transitionMatrix[prev_tag][tag] = transitionMatrix[prev_tag][tag] + 1
					else :
						transitionMatrix[prev_tag][tag] = dict()
						transitionMatrix[prev_tag][tag] = 1
				else :
					transitionMatrix[prev_tag] = dict()
					transitionMatrix[prev_tag][total] = 0
					transitionMatrix[prev_tag][tag] = dict()
					transitionMatrix[prev_tag][tag] = 1
				transitionMatrix[prev_tag]["total"] = transitionMatrix[prev_tag]["total"] + 1
			#adding to wordToTags
			if word not in wordToTags :
				wordToTags[word] = dict()
				wordToTags[word][tag] = 1
			else :
				if tag in wordToTags[word] :
					wordToTags[word][tag] = wordToTags[word][tag] + 1
				else :
					wordToTags[word][tag] = 1
			#adding to emissionMatrix 
			if tag not in emissionMatrix:
				emissionMatrix[tag] = dict()
				emissionMatrix[tag]["total"] = 0
				emissionMatrix[tag][word] = dict()
				emissionMatrix[tag][word] = 1
			else :					
				if word in emissionMatrix[tag] :
					emissionMatrix[tag][word] = emissionMatrix[tag][word] + 1
				else :
					emissionMatrix[tag][word] = dict()
					emissionMatrix[tag][word] = 1
			emissionMatrix[tag]["total"] = emissionMatrix[tag]["total"] + 1
			count = count + 1 
	sentenceCount = sentenceCount + 1
symbolFrequency["\""] = 1;
symbolFrequency["\\"] = 1;
symbolFrequency["_"] = 1;
symbolFrequency[" "] = 865398;
#1153864*.75 = 865398
tM = open("./hmmModel/TransitionProbabilities.txt","wb+");
eM = open("./hmmModel/EmissionProbabilities.txt","wb+");
sM = open("./hmmModel/StartingProbabilities.txt","wb+");
wrds = open("./hmmModel/Words.txt","wb+");
w2Tags = open("./hmmModel/Words_TagsTaken.txt","wb+");
tgs = open("./hmmModel/Tags.txt","wb+");
wrdsCount = open("./hmmModel/WordsWithCount.txt","wb+");
sf = open("hmmModel/symbolFrequency.txt","wb+");

tMj = dict();
eMj = dict();
sMj = dict();
wrdsj = dict();
w2Tagsj = dict();
wrdsCountj = dict();
tgsj = dict();
sfj = dict();

tMj["TransitionProbabilities"] = transitionMatrix;
eMj["EmissionProbabilities"] = emissionMatrix;
sMj["StartingProbabilities"] = startingMatrix;
wrdsj["Words"] = wordToTags.keys();
w2Tagsj["Words_and_TagsTaken"] = wordToTags;
tgsj["Tags"] = emissionMatrix.keys();
sfj["SymbolFrequency"] = symbolFrequency;
wrdsCountj["Words_Count"] = wordFrequency;

json.dump(tMj, tM,-1,indent=4,separators=(',',':') );
json.dump(eMj, eM,-1,indent=4,separators=(',',':') );
json.dump(sMj, sM,-1,indent=4,separators=(',',':') );
json.dump(wrdsj, wrds, -1,indent=4,separators=(',',':') )
json.dump(w2Tagsj, w2Tags, -1,indent=4,separators=(',',':') )
json.dump(tgsj, tgs, -1,indent=4,separators=(',',':') )
json.dump(sfj,sf, -1,indent=4,separators=(',',':') )
json.dump(wrdsCountj, wrdsCount, -1,indent=4,separators=(',',':') )





print "TransitionMatrix = ",transitionMatrix
print "wordToTags = ",wordToTags
print "emissionMatrix = ",emissionMatrix
fullIndex["transitionMatrix"] = transitionMatrix
fullIndex["wordToTags"] = wordToTags
fullIndex["emissionMatrix"] = emissionMatrix
fullIndex["sentenceStartWordsToTags"] = sentenceStartWordsToTags
fullIndex["wordFrequencies"] = wordFrequency
fullIndex["startingMatrix"] = startingMatrix
fullIndex["symbolFrequency"] = symbolFrequency
print "Dumping Index" 
json.dump(fullIndex, completeIndexFile,-1,indent=4,separators=(',',':') )
print "Index Dumped"
print "Correct Sentence Count = ",cSentenceCount;
print "symbolFrequency = ",symbolFrequency
print "Number of symbols = ", len(symbolFrequency.keys())
maxFreq = 0;
for key in symbolFrequency.keys() :
	if maxFreq < symbolFrequency[key] :
		maxFreq = symbolFrequency[key]
print "maxFreq = ",maxFreq	
#print "snetence delimiters frequencies  :", sentenceEnd
#print "sentenceEnding = ",lSentenceEnd
print "Number of words = ", len(wordFrequency.keys())
print "Length of wList = ", len(wList)
print "Number of Words = ", wCount
print "mCOunt = ",mCount
#print "Number of different tags = ", len(tList)

