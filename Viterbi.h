#ifndef VITERBI_H
#define VITERBI_H

#include<string>
#include<fstream>
#include "HuffmanCoding.h"
#include "TokenProcessor.h"
#include "Types.h"

using namespace std;
class Viterbi
{
	private : 
		HuffmanCoding * hCode;
		TokenProcessor tp;
		double ** dpm;
		maxValue ** argMax;
		int tagSetSize;
	public :
		/**
		  * returns true if corrected completely otherwise false.
		  * If false, fills in the rest of the input arguments with relevant values.	
		  */	
		Viterbi();
		~Viterbi();
		int correctTheSentence(const string & inSentence, string & inInducedBitSequence, string & errorBitSequence, string & errorSentence, int count);
		void clearMatrices(int tagSetSize, int numberOfBits );
};
#endif	
