#include<string>
#include<vector>
#include<map>
#include<fstream>
#include "json/json.h"

using namespace std;

class HuffmanCoding {
	private :
		
		class hNode {
			public :
				string s;
				int frequency;
				hNode * left;
				hNode * right; 	
				hNode(string s, int frequency, hNode * left = NULL, hNode * right = NULL);
		}; 
		
		class llNode {
			public :
				hNode * node;
				llNode * next;
				llNode(hNode * inNode, llNode * inNext);
		};

		void initialize();
		HuffmanCoding();
		HuffmanCoding(HuffmanCoding const& copy);            // Not Implemented
        	HuffmanCoding& operator=(HuffmanCoding const& copy); // Not Implemented
		static HuffmanCoding * mInstance;
	public :
		typedef std::map<std::string, std::string> symbolCodesMap;
		typedef std::map<std::string, int> symbolFrequencyMap;

		static HuffmanCoding *  getInstance(){
			if( mInstance == NULL)
	            		mInstance = new HuffmanCoding() ;
            		return mInstance;
        	}	
	
		static void freeInstance(){
			if(mInstance){
		
				delete mInstance;
				mInstance = NULL;
			}
		}	
		~HuffmanCoding();
		void print(hNode **a, int size);
		void printLL(llNode * root);
		void printCodes(symbolCodesMap * symbolCodes);
		void quickSort(hNode ** a, int start, int end);
		int partition(hNode ** a, int start, int end);
		void populateHNode();
		void clearHNodeArray();
		void createLinkedList();
		void createTree();
		int sizeOfQueue();
		void generateCodes(hNode * root, string s);
		void retrieveSymbols();
		int getLengthOfLargestToken( string &word );
		void split( const string & word, const string & delimiter, vector<string> & wordList);
		void encode(const string & inWord, string & outBitSequence);
		void decode(const string & inBitSequence, string & outWord);
		void encodeAndInduceErrors(fstream & inFile, fstream & outFile);
		void encodeAndInduceErrors(const string & inFile, string & outFile);	
		void dumpWordList();
		hNode ** hNodeArray;
		llNode * root;
		int symbolCount;
		int sSize;
		symbolCodesMap symbolCodes;
		symbolFrequencyMap symbolFrequency;
		int PEB;
};
