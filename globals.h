#ifndef GLOBALS_H
#define GLOBALS_H

#include<fstream>
//Define the -DP compilation argument to enable logging
#ifdef DP
	#define LOG(x) do { \
			cout<<x; \
			} while(0)
#else
	#define LOG(x)
#endif

static std::fstream logFile("./log");

#endif
