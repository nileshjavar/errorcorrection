#include "globals.h"
#include<fstream>
#include<iostream>
#include<string>
#include<sys/time.h>
#include "Viterbi.h"
#define MAX_BITS 5000
using namespace std;

Viterbi::Viterbi(){
	hCode = HuffmanCoding::getInstance();
	dpm = NULL;
	argMax = NULL;
	tagSetSize = (tp.masterTagsList).size();
	dpm  = new double * [tagSetSize];
	argMax = new maxValue * [tagSetSize];	
	for(int i = 0;i<tagSetSize;i++){
		dpm[i] = new double[MAX_BITS];
		for(int j = 0;j<MAX_BITS;j++){
			if( j == 0)
				dpm[i][j] = 1;
			else
				dpm[i][j] = 0;
		}
		argMax[i] = new maxValue[MAX_BITS];
	}
}

Viterbi::~Viterbi(){
	clearMatrices(tagSetSize, MAX_BITS);
}
int Viterbi::correctTheSentence(const string & inSentence, string & errorInducedBitSequence, string & errorBitSequence, string & errorSentence, int count)
{
	struct timeval tim;
	gettimeofday(&tim, NULL);
	double startTime = tim.tv_sec;
	double endTime = tim.tv_sec;
	//LOG("Calling correct the sentence"<<endl);
	LOG(inSentence<<endl);
	hCode->encodeAndInduceErrors(inSentence, errorBitSequence);
	if(errorBitSequence.size() > MAX_BITS ){
		LOG("Size of sentence greater than "<<MAX_BITS<<endl);
		errorSentence = "OUT OF MEMORY";
	}
	errorInducedBitSequence = errorBitSequence;
	LOG(errorBitSequence<<endl);
	string temp("");
	string line(errorBitSequence);
        if(errorBitSequence.size() == 0 )
	{
		LOG("Could not induce errors input Sequence"<<endl);
		errorSentence = "ERROR";
		return 0;
	}		
	vector<string> tagList = tp.masterTagsList;
	vector<string> tagVec;
	int tagSetSize = tagList.size();
	int numberOfBits = line.size()+1;
	vector<string> tokens;
	//dpm  = new double * [tagSetSize];
	//argMax = new maxValue * [tagSetSize];	
	for(int i = 0;i<tagSetSize;i++){
		//LOG("Processing bit : "<<i<<"/"<<numberOfBits<<endl);
		//dpm[i] = new double[numberOfBits];
		for(int j = 0;j<numberOfBits;j++){
			if( j == 0)
				dpm[i][j] = 1;
			else
				dpm[i][j] = 0;
		}
		//argMax[i] = new maxValue[numberOfBits];
		argMax[i]->clear();
	}
	//LOG("Starting the dp"<<endl);
	string bitSequence("");
	double prob = 0;
	double maxProb = 0;	
	double maxProbAllBitSeq = 0;
	int maxTagIndex = -1;
	string bs("");
	maxValue argMaxInstance;
	vector< wordTagListBitSequence * > wtInfo;
	int j = 0;
	//LOG("Started the DP"<<endl);
	//LOG("TagSetSize = "<<tagSetSize<<endl);
	//LOG("Line Size = "<<line.size()<<endl);
	vector<int> tagIndices;	
	vector<string> lastTagList;
	for(int bit = 1;bit < numberOfBits; bit++){
		/*gettimeofday(&tim, NULL);
		endTime = tim.tv_sec;
		if(endTime - startTime > 20) 
		{
			errorSentence = "Time out";
			return 1;
		}*/
		j = bit-1;
		//LOG("Just entered : "<<tagIndex<<endl);
		argMaxInstance.clear();	
		bitSequence = "";
		maxProbAllBitSeq = 0;
		LOG("Count = "<<count<<" Processing Bit = "<<bit<<" "<<"Number of bits = "<<numberOfBits<<" "<<endl);
		while( j>=0 && j> bit - 70){
			/*gettimeofday(&tim, NULL);
			endTime = tim.tv_sec;
			if(endTime - startTime > 45) 
			{
				errorSentence = "Time out";
				return 1;
			}*/

			//LOG("Processing bit : "<<bit<<"/"<<numberOfBits<<" j = "<<j<<"/"<<bit-20<<endl);
			//clearing the wtInfo memory
			//LOG("Clearing the wtInfo"<<endl);
			for(int wtInfoItr = 0; wtInfoItr<wtInfo.size();wtInfoItr++){
				if(wtInfo[wtInfoItr]){
					delete 	wtInfo[wtInfoItr];
					wtInfo[wtInfoItr] = NULL;
				}
			}
			wtInfo.clear();
			//LOG("After clearing the wtInfo"<<endl);
			bitSequence = line.substr(j,bit-j);
			//LOG("Processing bit : "<<bit<<"/"<<numberOfBits<<" j = "<<j<<"/"<<bit-20<<" bitSequence = "<<bitSequence<<endl);
			//LOG("Getting the wtInfo : line :: "<<bitSequence<<endl);
			tp.createTokens(bitSequence, tokens, wtInfo );
			//LOG("Size of wtInfo = "<<wtInfo.size());
			if(wtInfo.size() > 0){
				/*
				//Prints the WTList
				for( int i = 0; i< wtInfo.size();i++){
					LOG("i = "<<i<<" "<<"bitSequence = "<<wtInfo[i]->bitSequence<<" "<<endl);
;
					for(int j = 0; j< wtInfo[i]->words.size();j++){
						LOG("Word = "<<(wtInfo[i]->words)[j]<<" tags = ");
						for(int k = 0; k< (wtInfo[i]->tagList)[j].size();k++ )
							LOG((wtInfo[i]->tagList)[j][k]<<" ");
					}
				}*/
				//LOG(endl);
				for( int i = 0;i<wtInfo.size();i++){
					/*gettimeofday(&tim, NULL);
					endTime = tim.tv_sec;
					if(endTime - startTime > 45) 
					{
						errorSentence = "Time out";
						return 1;
					}*/
					vector<int> tagIndices;
					tagIndices.clear();
					vector<string> lastTagList;
					lastTagList.clear();
					lastTagList = (wtInfo[i]->tagList)[(wtInfo[i]->tagList).size()-1];
					for(int lastTagListIndex = 0;lastTagListIndex<lastTagList.size();lastTagListIndex++){
						for( int tagIndexTemp = 0; tagIndexTemp< tagSetSize;tagIndexTemp++){
							if( tagList[tagIndexTemp] == lastTagList[lastTagListIndex])
								tagIndices.push_back(tagIndexTemp);
						}
					}

					int tagIndex;
					for( int tagIndicesItr = 0;tagIndicesItr<tagIndices.size();tagIndicesItr++){
						gettimeofday(&tim, NULL);
						endTime = tim.tv_sec;
						if(endTime - startTime > 45) 
						{
							errorSentence = "Time out";
							return 1;
						}
						tagIndex = tagIndices[tagIndicesItr];
						prob = 0;
						maxProb = 0;
						maxTagIndex = 0;
						for( int prevTagIndex = 0; prevTagIndex < tagSetSize;prevTagIndex++){
						//optimization
							if(dpm[prevTagIndex][j] > 0){
								tagVec.clear();
								//LOG("i = "<<i<<" calling WT Sequence"<<endl);
								tp.findWTSequence( wtInfo[i]->words,
										   wtInfo[i]->tagList,
									           (j == 0 ? "" : tagList[prevTagIndex]),
										   tagList[tagIndex],
										   tagVec,
										   0,
										   prob);
								if( prob > maxProb){
									bs = wtInfo[i]->bitSequence;
									maxProb = prob;
									maxTagIndex = prevTagIndex;
								}	
							}
						}
						if(maxProb*dpm[maxTagIndex][j] > dpm[tagIndex][bit]){
							//LOG("row = "<<tagIndex<<" column = "<<bit<<" mvRow = "<<maxTagIndex<<" mvColumn = "<<j<<" bs = "<<bs<<" prob = "<<dpm[tagIndex][bit]<<" maxProb = "<<maxProb*dpm[maxTagIndex][j]<<endl);
							dpm[tagIndex][bit] = maxProb*dpm[maxTagIndex][j];
							argMax[tagIndex][bit].row = maxTagIndex;
							argMax[tagIndex][bit].column = j;
							argMax[tagIndex][bit].bitSequence = bs;
						}		
					}
				}
			}
			j--;
		}
	}
	
	
	//LOG("Backtracking to get the correct bits"<<endl);
	//ArgMax backtracking to retrieve teh correct sequence
	//LOG("Number of Bits = "<<numberOfBits<<endl);
	int btBitIndex = numberOfBits-1;
	int btTagIndex = -1;
	double btMaxProb = 0;
	string correctBits("");
	//LOG("Starting"<<endl);
	for(int i = 0;i<tagSetSize;i++){
		if( dpm[i][btBitIndex] > btMaxProb){
			//LOG("Greater :: Row = "<<i<<endl);
			btMaxProb = dpm[i][btBitIndex];
			btTagIndex = i; 
		} else if( dpm[i][btBitIndex] == btMaxProb ){
			//LOG("Equal :: Row = "<<i<<endl);
		}
	}
	if(btTagIndex == -1 ){
		LOG(" probabilities of last bit == 0 becuase of unknown word.Exiting"<<endl);
		errorSentence = "probabilities of last bit == 0";	
		return 0;
	}
	//LOG("Ending. Again Starting"<<endl);
	int tempBtBitIndex = -1,tempBtTagIndex = -1;
	LOG("row = "<<btTagIndex<<" column = "<<btBitIndex<<endl);
	string tempBtStr("");
	while(btBitIndex!=0){
		tempBtStr ="";
		LOG(argMax[btTagIndex][btBitIndex].bitSequence<<endl);
		hCode->decode(argMax[btTagIndex][btBitIndex].bitSequence,tempBtStr);
		//LOG("row = "<<btTagIndex<<" column = "<<btBitIndex<<" word = "<<tempBtStr<<" bs = "<<argMax[btTagIndex][btBitIndex].bitSequence<<" prob = "<<dpm[btTagIndex][btBitIndex]<<endl);
		
		correctBits = argMax[btTagIndex][btBitIndex].bitSequence + correctBits;
		tempBtBitIndex =  argMax[btTagIndex][btBitIndex].column;
		tempBtTagIndex = argMax[btTagIndex][btBitIndex].row;		
		btBitIndex = tempBtBitIndex;
		btTagIndex = tempBtTagIndex;
	}
	LOG("Ending"<<endl);	
	//correctBits = '1'+correctBits;	
	//LOG("Size of corrected Bits = "<<correctBits.size());
	//LOG("Size of input = "<<numberOfBits-1<<endl);
	//LOG(correctBits<<endl);
	//LOG(line<<endl);	
	
	string decodedStr1("");
	if( correctBits.size()!= numberOfBits-1)
	{
		LOG("Massive error has occurred. Input and output size not same."<<endl);
		errorSentence = "Input and output size not same";
		//clearMatrices(tagSetSize, numberOfBits);
		return 0;
	}
	//LOG("Started decoding"<<endl);	
	string decodedStr("");
	hCode->decode(correctBits, decodedStr);
	//LOG("finised decoding"<<endl);	
	if( inSentence == decodedStr){
		//clearMatrices(tagSetSize, numberOfBits);
		return 2;
	} else {
		errorSentence = decodedStr;
		errorBitSequence = correctBits;
		//clearMatrices(tagSetSize, numberOfBits);
		return 0;
	}
}

void Viterbi::clearMatrices( int tagSetSize, int numberOfBits)
{
	LOG("Clearing the matrix"<<endl);
	for(int i =0;i<tagSetSize;i++){
		if(dpm[i]){
			delete [] dpm[i];
			dpm[i] = NULL;
		}
		if(argMax[i]){
			delete [] argMax[i];
			argMax[i] = NULL;
		}
	}
		
	if(dpm!=NULL){
		delete [] dpm;
		dpm = NULL;
	}

	if(argMax!=NULL){
		delete [] argMax;
		argMax = NULL;
	}
}
